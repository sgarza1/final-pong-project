If using motion capture

The 1st object you select will link with paddle 1 (left paddle)
The 2nd object you select will link with paddle 2 (right paddle)

View: Handles GUI Display
-Interface Class

Model: Contains Game Data (position, velocity, size, etc). for each object. Controls Events/Responses

Consists of of the following classes:


-Ball
-Monster
-Paddle
-Portal
-Cloud
-Obstacle
-Gravity
-Pong (controls game flow)

Controller: Provides inputs to the model
-StandardController Class
-MotionController Class


